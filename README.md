This Assignment was created using maven in Intellij.
This Assignment is structured in two main files-packages.
The first one is the main/java/Assignment_1 which contains all the classes
that the app uses in order to create the heroes and items.
To be more specific, the Hero and the Heroattribute classes are in the HeroClasses package.
Let's begin with the Hero class, which is an abstract parent class with all the
implementing methods using the interfaces located in the HeroesInterface package.
The protected fields for this class are: A string name for the name of the Hero, an int level 
for the level, HeroAttribute heroAttributes, which is a class to store the 3 encapsulated 
hero attributes plus a method(increaseAttributes) to increase attributes,
a HashMap<Slot,Item> equipment to store the equipment and finally two lists storing the 
valid weapons and armors of each Hero. I include a constructor to initialize all the above
fields and to create heroes with the only parameter a name. Next we have the method LevelUp()
to increase the level of our hero by 1. In each child class we have the same method which
overrides the parent giving specific attributes in each child.
We also have the same method overloading with an int levels parameter if we want
to increase the level of our hero with more than 1. We use equipWeapon() 
with a Weapon weapon parameter in order to equip a weapon, and equipArmor() to equip an armor.
Both methods throw a custom Exception when the level of the hero is lower that the
requiredLevel or if the type of the weapon doesn't match the valid types of weapons or armors.
I include also a method to return the damage a hero can cause based on his weapon and a specific
attribute,unique for him, using the formula in the assignment description. 
Last but not least, I have also included a method, totalAttributes(),
in order to find the total attributes our hero has. To see all the information of a hero,
we need to use the display() method.
In the HeroesClasses package we can find the classes to create 4 Heroes.
All of them inherit all the fields and methods from the class Hero. In the constructor
of each class we give specific values to the attributes and to the valid weapon and armor list. 
We override some parent methods to give unique character to each hero.
As mentioned and above, we have the package HeroesInterfaces with the interfaces for the 
Hero class and a package with name ItemsInterfaces with interfaces for the
Item,Armor and Weapon classes. The last package we have is the ItemsAndEquipment.
Contains the Item,Armor and Weapon class. Item is the abstract parent class of the other
two classes. In Item we have as protected fields 
a name, a requiredLevel, a Slot slot which is the slot that the item will be hold 
and Slot represented as an enumerator with the values Weapon, Head, Body and Legs. In the Item
class we have some getters methods to achieve encapsulation.
In Weapon class we have a constructor calling the super constructor from Item, and 
getters method to encapsulate the unique fields weaponType and weaponDamage. We have also 
an enumerator enum WeaponType with the various types of weapon.
In Armor class the logic is the same as the Weapon class, with the only different that we 
don't have a weapon damage as field but a HeroAttribute armorAttribute which basically
gives the armor some attributes to stronger the hero. This armorAttribute is used in the 
totalAttributes method previously mention in Hero class.
The second main file-package is in test/java/TestsForAssignment1 with all the tests 
that are required from the assignment document. We have 6 total Classes, represent each
class we have, except of course the abstract classes Item and Hero. All the test-methods
are well named and easy to understand.






