package Assignment_1;

import Assignment_1.Exceptions.InvalidArmorException;
import Assignment_1.Exceptions.InvalidWeaponException;
import Assignment_1.HeroesClasses.Warrior;
import Assignment_1.ItemsAndEquipment.Armor;
import Assignment_1.ItemsAndEquipment.Armor.ArmorType;
import Assignment_1.ItemsAndEquipment.Item.Slot;
import Assignment_1.ItemsAndEquipment.Weapon;
import Assignment_1.ItemsAndEquipment.Weapon.WeaponType;

// Using this main only to be sure that all functions are working properly.
public class Main {
    public static void main(String[] args) throws InvalidWeaponException, InvalidArmorException {
        Weapon weapon = new Weapon("Common Axe",1, Slot.Weapon, WeaponType.Axe,2);
        Armor armor = new Armor("Common Plate Chest",1, Slot.Body, ArmorType.Plate,1,0,0);
        Warrior warrior = new Warrior("Kostas");
        warrior.equipWeapon(weapon);
        warrior.equipArmor(armor);
        warrior.display();
    }
}
