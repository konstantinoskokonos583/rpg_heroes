package Assignment_1.HeroClasses;

// this class is for the Hero attributes
// we have many getters and setters to achieve encapsulation
//for getting and setting the 3 attributes separate.
// at the end of this class we have a method increaseAttributes that increases
// the attributes based on 3 parameters
// double amountOfStrength, double amountOfDexterity and double amountOfIntelligence
// We use this method in LevelUp method and in totalAttributes.

public class HeroAttribute {
    private double Strength; //determines the physical strength of the character
    private double Dexterity; //determines the characters ability to attack with speed and nimbleness
    private double Intelligence; //determines the characters affinity with magic

    public double getStrength() {
        return Strength;
    }

    public void setStrength(double strength) {
        Strength = strength;
    }

    public double getDexterity() {
        return Dexterity;
    }

    public void setDexterity(double dexterity) {
        Dexterity = dexterity;
    }

    public double getIntelligence() {
        return Intelligence;
    }

    public void setIntelligence(double intelligence) {
        Intelligence = intelligence;
    }

    public void increaseAttributes(double amountOfStrength, double amountOfDexterity, double amountOfIntelligence){
        Strength += amountOfStrength;
        Dexterity += amountOfDexterity;
        Intelligence += amountOfIntelligence;
    }
}
