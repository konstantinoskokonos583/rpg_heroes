package Assignment_1.HeroClasses;

import Assignment_1.Exceptions.InvalidArmorException;
import Assignment_1.Exceptions.InvalidWeaponException;
import Assignment_1.HeroesInterfaces.*;
import Assignment_1.ItemsAndEquipment.Armor;
import Assignment_1.ItemsAndEquipment.Armor.ArmorType;
import Assignment_1.ItemsAndEquipment.Item;
import Assignment_1.ItemsAndEquipment.Item.Slot;
import Assignment_1.ItemsAndEquipment.Weapon;
import Assignment_1.ItemsAndEquipment.Weapon.WeaponType;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class Hero implements LevelUpHero, EquipAWeapon, EquipAnArmor, CalculateDamage, CalculateTotalDamage, CalculateTotalAttributes, InformationAboutHero {
    protected String name; // name of the Hero
    protected int level; // level of the Hero
    //hero attributes
    protected HeroAttribute heroAttributes= new HeroAttribute();
    //holds currently equipped items
    protected HashMap<Slot,Item> equipment= new HashMap<>();
    //a list of weapon types a hero can equip based on their subclass
    protected ArrayList<WeaponType> validWeaponTypes= new ArrayList<>();
    //- a list of armor types a hero can equip based on their subclass
    protected ArrayList<ArmorType> validArmorTypes= new ArrayList<>();

    public String getName() {
        return name;
    } // get method to return the name
    public int getLevel() {
        return level;
    } // get method to return the level
    // get method to return hero attributes
    public HeroAttribute getHeroAttributes() {
        return heroAttributes;
    }
    // get method to return a hashmap with the Items the Hero is equipped
    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }
    // method to return the valid weapon list
    public ArrayList<WeaponType> getValidWeaponTypes() {
        return validWeaponTypes;
    }
    // method to return the valid Armor list
    public ArrayList<ArmorType> getValidArmorTypes() {
        return validArmorTypes;
    }

    //Constructor – each hero is created by passing just a name.
    protected Hero(String name){
        this.name=name;
        this.level = 1; //All heroes start at level 1
        // We fill the equipment hashmap with the slots as keys and the Items as values(default null)
        for (Slot slot : Slot.values()){
            equipment.put(slot,null);
        }
    }
    //Increases the level of a character by 1
    // and increases their LevelAttributes in the child class with overrrid
    public void LevelUp(){
        this.level += 1;
    }
    //Increases the level of a character by levels
    // and increases their LevelAttributes in the child class with overrride
    public void LevelUp(int levels){
        this.level += levels;
    }
    //Equipping weapon
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        // we check if the type of the item is in the valid weapon types list
        if(!(validWeaponTypes.contains(weapon.getWeaponType()))) // if not
            // we throw the custom Exception with a message to understand the problem
            throw new InvalidWeaponException("This Hero can't equip this type of weapon!");
        // we check if the hero level is bigger or equal to the required level of the weapon
        if(!(this.level >= weapon.getRequiredLevel())) //if not
            // we throw the custom Exception with a message to understand the problem
            throw new InvalidWeaponException("Weapon's level is too high for the Hero!");
        // if we pass the above if statements then we proceed to the equipment of the weapon
        if(validWeaponTypes.contains(weapon.getWeaponType()) && (this.level >= weapon.getRequiredLevel()))
            //we replace the weapon we want to equip in the correct Slot
            this.equipment.replace(weapon.getSlot(),weapon);

    }
    public void equipArmor(Armor armor) throws InvalidArmorException { //Equipping armor
        // we check if the type of the item is in the valid armor types list
        if(!(validArmorTypes.contains(armor.getArmorType()))) // if not
            // we throw the custom Exception with a message to understand the problem
            throw new InvalidArmorException("This Hero can't equip this type of armor!");
        // we check if the hero level is bigger or equal to the required level of the armor
        if(!(this.level >= armor.getRequiredLevel())) //if not
            // we throw the custom Exception with a message to understand the problem
            throw new InvalidArmorException("Armor's level is too high for the Hero!");
        // if we pass the above if statements then we proceed to the equipment of the armor
        if(validArmorTypes.contains(armor.getArmorType()) && (this.level >= armor.getRequiredLevel()))
            //we replace the armor we want to equip in the correct Slot
            this.equipment.replace(armor.getSlot(),armor);
    }
    //damage is calculated on the fly and not stored
    public double damage(){ // we use the same method in every child class but override
        double WeaponDam;
        if((this.equipment.get(Slot.Weapon)) == null) { // we check to see if we have weapon or not
            WeaponDam = 1.0; //if we don't have weapon damage is equal to 1
        } else {
            //else we extract the weapon from the equipment hashmap
            // and using the getWeaponDamage to find the weapon damage
            // in the child class we will use this WeaponDam using super
            // to calculate the final weapon damage
            WeaponDam = ((Weapon)this.equipment.get(Slot.Weapon)).getWeaponDamage();
        }
        return WeaponDam;
    }
    //calculated on the fly and not stored
    //this method calculates the totalAttributes a hero can have based on hero attributes and
    // the attributes from armor
    public HeroAttribute totalAttributes(){
        // we create a new Heroattribute to store the final attributes
        HeroAttribute finalAttributes = new HeroAttribute();
        // we iterate the hashmap by Slot-key and if the item is not null/ has an actual Item in this Slot
        // and the slot is not Weapon, then we increase the new heroAttribute with the values we get
        // from each Item using get methods.
        for (Slot s : Slot.values()){
            if((this.equipment.get(s)) != null && !(s.equals(Slot.Weapon))){
                finalAttributes.increaseAttributes(
                        ((Armor)this.equipment.get(s)).getArmorAttribute().getStrength(),
                        ((Armor)this.equipment.get(s)).getArmorAttribute().getDexterity(),
                        ((Armor)this.equipment.get(s)).getArmorAttribute().getIntelligence());
            }
        }
        // finally after we finish with the armor attributes we increase the
        // HeroAttribute finalAttributes with the attributes of the Hero (with or without level)
        // and return the result(finalAttributes)
        finalAttributes.increaseAttributes(
                this.heroAttributes.getStrength(),
                this.heroAttributes.getDexterity(),
                this.heroAttributes.getIntelligence());
        return finalAttributes;
    }
    //details of Hero to be displayed
    // we use StringBuilder to form all the information
    // of the Hero to one string, and we print the result
    public void display() {
        StringBuilder str = new StringBuilder();
        str.append("We have a Hero with the following state:\n");
        str.append("Name: "+this.name+"\n");
        str.append("Class: "+this.getClass().getSimpleName()+"\n");
        str.append("Level: "+this.level+"\n");
        str.append("Total Strength: "+totalAttributes().getStrength()+"\n");
        str.append("Total Dexterity: "+totalAttributes().getDexterity()+"\n");
        str.append("Total Intelligence: "+totalAttributes().getIntelligence()+"\n");
        str.append("Damage: "+this.damage());
        System.out.println(str);
    }
}
