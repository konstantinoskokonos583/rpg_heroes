package Assignment_1.Exceptions;

// this class represent the custom exception for when we equip an Armor that has wrong
// valid type or the required level is not lower or equal to level of the Hero class.
public class InvalidArmorException extends Exception{
    public InvalidArmorException() {}

    public InvalidArmorException(String output)
    {
        super(output);
    }
}
