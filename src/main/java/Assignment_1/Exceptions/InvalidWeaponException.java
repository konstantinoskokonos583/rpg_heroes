package Assignment_1.Exceptions;

// this class represent the custom exception for when we equip a Weapon that has wrong
// valid type or the required level is not lower or equal to level of the Hero class.
public class InvalidWeaponException extends Exception{
    public InvalidWeaponException() {}

    public InvalidWeaponException(String output)
    {
        super(output);
    }
}
