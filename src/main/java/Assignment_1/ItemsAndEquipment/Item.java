package Assignment_1.ItemsAndEquipment;

import Assignment_1.ItemsInterfaces.*;

// This is an abstract parent class for Items
public abstract class Item implements ItemGetName, ItemGetRequiredLevel, ItemGetSlot {
    protected String name;
    protected int requiredLevel;
    public enum Slot{Weapon,Head,Body,Legs}
    protected Slot slot;

    // We have a constructor with parameters a name, the required level and a slot
    protected Item(String name,int requiredLevel,Slot slot){
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }
    // We encapsulate all the fields using getters
    public String getName() {
        return this.name;
    }
    public int getRequiredLevel(){
        return this.requiredLevel;
    }
    public Slot getSlot(){
        return this.slot;
    }
}
