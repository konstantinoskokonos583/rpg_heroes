package Assignment_1.ItemsAndEquipment;

import Assignment_1.ItemsInterfaces.WeaponGetWeaponDamage;
import Assignment_1.ItemsInterfaces.WeaponGetWeaponType;

// This is the class for creating a Weapon which inherited
// all the fields and methods from Item class.
// As additional fields we have
// an enumerator WeaponType, a specific weaponType
// and the weapon damage.
public class Weapon extends Item implements WeaponGetWeaponType, WeaponGetWeaponDamage {
    public enum WeaponType{Axe,Bow,Dagger,Hammer,Staff,Sword,Wand}
    private WeaponType weaponType;
    private double weaponDamage;

    // Constructor using the constructor of the parent class Item (super())
    //and setting the specific weaponType and the damage for the weapon
    // Because for the weapon the slot is always Weapon,
    // I decided not to use another constructor for Item not having the parameter Slot
    // but to give always in the super constructor the Slot.Weapon regardless the Slot slot.
    public Weapon(String name,int requiredLevel,Slot slot, WeaponType weaponType,double weaponDamage){
        super(name,requiredLevel,Slot.Weapon);
        this.weaponType = weaponType;
        this.weaponDamage = weaponDamage;
    }
    // encapsulating the additional fields specific for the Weapon class.
    public WeaponType getWeaponType() {
        return weaponType;
    }
    public double getWeaponDamage() {
        return weaponDamage;
    }
}
