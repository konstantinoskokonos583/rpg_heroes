package Assignment_1.ItemsAndEquipment;

import Assignment_1.HeroClasses.HeroAttribute;
import Assignment_1.ItemsInterfaces.ArmorGetArmorAttributes;
import Assignment_1.ItemsInterfaces.ArmorGetArmorType;

// This is the class for creating an Armor which inherited
// all the fields and methods from Item class.
// As additional fields we have
// an enumerator ArmorType, a specific armorType
// and a HeroAttribute armorAttribute which contains all the specific armor attributes.

public class Armor extends Item implements ArmorGetArmorType, ArmorGetArmorAttributes {
    public enum ArmorType{Cloth,Leather,Mail,Plate}
    private ArmorType armorType;
    private HeroAttribute armorAttribute;

    // Constructor using the constructor of the parent class Item (super())
    //and setting the specific armorType and all the armor attributes
    public Armor(String name, int requiredLevel, Slot slot, ArmorType armorType,double amountOfStrength, double amountOfDexterity, double amountOfIntelligence){
        super(name,requiredLevel,slot);
        this.armorType=armorType;
        this.armorAttribute=new HeroAttribute();
        armorAttribute.setStrength(amountOfStrength);
        armorAttribute.setDexterity(amountOfDexterity);
        armorAttribute.setIntelligence(amountOfIntelligence);

    }

    // encapsulating the additional fields specific for the Armor class.
    public ArmorType getArmorType() {
        return armorType;
    }
    public HeroAttribute getArmorAttribute() {
        return armorAttribute;
    }
}
