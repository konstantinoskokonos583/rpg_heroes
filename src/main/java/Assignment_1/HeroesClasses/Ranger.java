package Assignment_1.HeroesClasses;

import Assignment_1.HeroClasses.Hero;
import Assignment_1.ItemsAndEquipment.Armor.ArmorType;
import Assignment_1.ItemsAndEquipment.Weapon.WeaponType;

// This class is specific for the Ranger child
public class Ranger extends Hero {
    // we have a constructor calling the super constructor to have all the fields
    // from the parent Hero class. We set the specific attributes for the Ranger class
    // and add the  valid types of weapon and armor in the corresponding list.
    public Ranger(String name){
        super(name);
        heroAttributes.setStrength(1.0);
        heroAttributes.setDexterity(7.0);
        heroAttributes.setIntelligence(1.0);
        validWeaponTypes.add(WeaponType.Bow);
        validArmorTypes.add(ArmorType.Leather);
        validArmorTypes.add(ArmorType.Mail);
    }

    // We override the LevelUps methods from parent and using super.LevelUp() to only
    // increase the field level by one or by levels as parameter.
    // Then using the increaseAttributes method we increase every attribute
    // with specific amounts of attributes as parameters only for Ranger class.
    @Override
    public void LevelUp(){
        super.LevelUp();
        heroAttributes.increaseAttributes(1.0,5.0,1.0);
    }
    @Override
    public void LevelUp(int levels) {
        super.LevelUp(levels);
        heroAttributes.increaseAttributes(levels * 1.0, levels * 5.0, levels * 1.0);
    }

    // for the damage because again its depends on a specific attribute for each child class
    // we override the parent method while using super.damage() to get the weapon damage
    // to use it for calculating the final damage.
    public double damage(){
        double totalDamage;
        totalDamage = (super.damage())*(1 + (totalAttributes().getDexterity())/100);
        return totalDamage;
    }
}
