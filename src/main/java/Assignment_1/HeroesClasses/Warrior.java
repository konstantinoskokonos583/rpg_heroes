package Assignment_1.HeroesClasses;

import Assignment_1.HeroClasses.Hero;
import Assignment_1.ItemsAndEquipment.Armor.ArmorType;
import Assignment_1.ItemsAndEquipment.Weapon.WeaponType;

// This class is specific for the Warrior child
public class Warrior extends Hero {

    // we have a constructor calling the super constructor to have all the fields
    // from the parent Hero class. We set the specific attributes for the Warrior class
    // and add the  valid types of weapon and armor in the corresponding list.
    public Warrior(String name){
        super(name);
        heroAttributes.setStrength(5.0);
        heroAttributes.setDexterity(2.0);
        heroAttributes.setIntelligence(1.0);
        validWeaponTypes.add(WeaponType.Axe);
        validWeaponTypes.add(WeaponType.Hammer);
        validWeaponTypes.add(WeaponType.Sword);
        validArmorTypes.add(ArmorType.Mail);
        validArmorTypes.add(ArmorType.Plate);
    }

    // We override the LevelUps methods from parent and using super.LevelUp() to only
    // increase the field level by one or by levels as parameter.
    // Then using the increaseAttributes method we increase every attribute
    // with specific amounts of attributes as parameters only for Warrior class.
    @Override
    public void LevelUp(){
        super.LevelUp();
        heroAttributes.increaseAttributes(3.0,2.0,1.0);
    }
    @Override
    public void LevelUp(int levels){
        super.LevelUp(levels);
        heroAttributes.increaseAttributes(levels * 3.0, levels * 2.0, levels * 1.0);
    }

    // for the damage because again its depends on a specific attribute for each child class
    // we override the parent method while using super.damage() to get the weapon damage
    // to use it for calculating the final damage.
    @Override
    public double damage(){
        double totalDamage;
        totalDamage = (super.damage())*(1 + (totalAttributes().getStrength())/100);
        return totalDamage;
    }
}
