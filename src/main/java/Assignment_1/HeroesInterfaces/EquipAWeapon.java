package Assignment_1.HeroesInterfaces;
import Assignment_1.Exceptions.InvalidWeaponException;
import Assignment_1.ItemsAndEquipment.Weapon;

public interface EquipAWeapon {
    void equipWeapon(Weapon weapon) throws InvalidWeaponException;
}
