package Assignment_1.HeroesInterfaces;

public interface LevelUpHero {
    void LevelUp();
    void LevelUp(int level);
}
