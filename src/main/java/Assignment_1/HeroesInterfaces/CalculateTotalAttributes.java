package Assignment_1.HeroesInterfaces;

import Assignment_1.HeroClasses.HeroAttribute;

public interface CalculateTotalAttributes {
    HeroAttribute totalAttributes();
}
