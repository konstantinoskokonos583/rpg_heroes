package Assignment_1.HeroesInterfaces;

import Assignment_1.Exceptions.InvalidArmorException;
import Assignment_1.ItemsAndEquipment.Armor;

public interface EquipAnArmor {
    void equipArmor(Armor armor) throws InvalidArmorException;
}
