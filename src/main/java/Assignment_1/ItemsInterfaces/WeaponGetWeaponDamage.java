package Assignment_1.ItemsInterfaces;

public interface WeaponGetWeaponDamage {
    public double getWeaponDamage();
}
