package Assignment_1.ItemsInterfaces;

import Assignment_1.ItemsAndEquipment.Armor;
import Assignment_1.ItemsAndEquipment.Armor.ArmorType;

public interface ArmorGetArmorType {
    public ArmorType getArmorType();
}
