package Assignment_1.ItemsInterfaces;

import Assignment_1.ItemsAndEquipment.Weapon.WeaponType;

public interface WeaponGetWeaponType {
    public WeaponType getWeaponType();
}
