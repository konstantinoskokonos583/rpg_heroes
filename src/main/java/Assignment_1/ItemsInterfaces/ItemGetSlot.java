package Assignment_1.ItemsInterfaces;

import Assignment_1.ItemsAndEquipment.Item;

public interface ItemGetSlot {
    public Item.Slot getSlot();
}
