package Assignment_1.ItemsInterfaces;

import Assignment_1.HeroClasses.HeroAttribute;

public interface ArmorGetArmorAttributes {
    HeroAttribute getArmorAttribute();
}
