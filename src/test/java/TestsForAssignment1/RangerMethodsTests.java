package TestsForAssignment1;

import Assignment_1.Exceptions.InvalidArmorException;
import Assignment_1.Exceptions.InvalidWeaponException;
import Assignment_1.HeroClasses.Hero;
import Assignment_1.HeroesClasses.Ranger;
import Assignment_1.ItemsAndEquipment.Armor;
import Assignment_1.ItemsAndEquipment.Item;
import Assignment_1.ItemsAndEquipment.Weapon;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class RangerMethodsTests {
    private final Hero hero = new Ranger("Kostas");
    @Test
    public void testConstructor(){
        Assertions.assertEquals(hero.getName(),"Kostas");
        Assertions.assertEquals(hero.getLevel(),1);
        Assertions.assertEquals(hero.getHeroAttributes().getStrength(),1.0);
        Assertions.assertEquals(hero.getHeroAttributes().getDexterity(),7.0);
        Assertions.assertEquals(hero.getHeroAttributes().getIntelligence(),1.0);
    }
    @Test
    public void testLevelUp(){
        hero.LevelUp();
        Assertions.assertEquals(hero.getLevel(),2);
        Assertions.assertEquals(hero.getHeroAttributes().getStrength(),2.0);
        Assertions.assertEquals(hero.getHeroAttributes().getDexterity(),12.0);
        Assertions.assertEquals(hero.getHeroAttributes().getIntelligence(),2.0);
    }

    @Test
    public void testEquipWeapon() throws InvalidWeaponException {
        Weapon weapon = new Weapon("Common Bow",1, Item.Slot.Weapon, Weapon.WeaponType.Bow,3);
        hero.equipWeapon(weapon);
        Assertions.assertEquals(((hero.getEquipment()).get(Item.Slot.Weapon)).getName(),"Common Bow");
    }

    @Test
    public void testEquipWeaponWithInvalidRequiredLevel() {
        Weapon weapon = new Weapon("Common Bow",2, Item.Slot.Weapon, Weapon.WeaponType.Bow,3);
        Assertions.assertThrows( InvalidWeaponException.class,() -> hero.equipWeapon(weapon));
    }

    @Test
    public void testEquipWeaponWithInvalidType() {
        Weapon weapon = new Weapon("Common Axe",1, Item.Slot.Weapon, Weapon.WeaponType.Axe,2);
        Assertions.assertThrows( InvalidWeaponException.class,() -> hero.equipWeapon(weapon));
    }

    @Test
    public void testEquipArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Mail",1, Item.Slot.Body, Armor.ArmorType.Mail,1,3,0);
        hero.equipArmor(armor);
        Assertions.assertEquals(((hero.getEquipment()).get(Item.Slot.Body)).getName(),"Common Mail");
    }

    @Test
    public void testEquipArmorWithInvalidRequiredLevel() {
        Armor armor = new Armor("Common Mail",2, Item.Slot.Body, Armor.ArmorType.Mail,1,3,0);
        Assertions.assertThrows( InvalidArmorException.class,() -> hero.equipArmor(armor));
    }

    @Test
    public void testEquipArmorWithInvalidType() {
        Armor armor = new Armor("Common Plate",1, Item.Slot.Body, Armor.ArmorType.Plate,1,3,0);
        Assertions.assertThrows( InvalidArmorException.class,() -> hero.equipArmor(armor));
    }

    @Test
    public void testTotalAttributesWithNoEquipment(){
        Assertions.assertEquals(hero.totalAttributes().getStrength(),1.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),7.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),1.0);
    }
    @Test
    public void testTotalAttributesWithOnePieceOfArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Mail",1, Item.Slot.Body, Armor.ArmorType.Mail,2,5,1);
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.totalAttributes().getStrength(),3.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),12.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),2.0);
    }

    @Test
    public void testTotalAttributesWithTwoPiecesOfArmor() throws InvalidArmorException {
        Armor armor1 = new Armor("Common Mail 1",1, Item.Slot.Body, Armor.ArmorType.Mail,2,5,1);
        Armor armor2 = new Armor("Common Mail 2",1, Item.Slot.Head, Armor.ArmorType.Mail,6,4,2);
        hero.equipArmor(armor1);
        hero.equipArmor(armor2);
        Assertions.assertEquals(hero.totalAttributes().getStrength(),9.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),16.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),4.0);
    }

    @Test
    public void testTotalAttributesWithReplacementOfArmor() throws InvalidArmorException {
        Armor armor1 = new Armor("Common Mail 1",1, Item.Slot.Head, Armor.ArmorType.Mail,2,5,1);
        Armor armor2 = new Armor("Common Mail 2",1, Item.Slot.Head, Armor.ArmorType.Mail,6,4,2);
        hero.equipArmor(armor1);
        hero.equipArmor(armor2);
        Assertions.assertEquals(hero.totalAttributes().getStrength(),7.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),11.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),3.0);
    }

    @Test
    public void testDamageWithNothing(){
        Assertions.assertEquals(hero.damage(),1.07);
    }
    @Test
    public void testDamageWithWeapon() throws InvalidWeaponException{
        Weapon weapon = new Weapon("Common Bow",1, Item.Slot.Weapon, Weapon.WeaponType.Bow,3);
        hero.equipWeapon(weapon);
        Assertions.assertEquals(hero.damage(),3.21);
    }
    @Test
    public void testDamageWithReplacementOfWeapon() throws InvalidWeaponException{
        Weapon weapon1 = new Weapon("Common Bow 1",1, Item.Slot.Weapon, Weapon.WeaponType.Bow,3);
        Weapon weapon2 = new Weapon("Common Bow 2",1, Item.Slot.Weapon, Weapon.WeaponType.Bow,4);
        hero.equipWeapon(weapon1);
        hero.equipWeapon(weapon2);
        Assertions.assertEquals(hero.damage(),4.28);
    }

    @Test
    public void testDamageWithArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Mail",1, Item.Slot.Head, Armor.ArmorType.Mail,6,4,2);
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.damage(),1.11);
    }

    @Test
    public void testDamageWithLevelUpAndArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Mail",1, Item.Slot.Head, Armor.ArmorType.Mail,6,4,2);
        hero.LevelUp();
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.damage(),1.16);
    }
    @Test
    public void testDamageWithWeaponAndArmor() throws InvalidWeaponException, InvalidArmorException {
        Weapon weapon = new Weapon("Common Bow",1, Item.Slot.Weapon, Weapon.WeaponType.Bow,3);
        Armor armor = new Armor("Common Mail",1, Item.Slot.Head, Armor.ArmorType.Mail,6,4,2);
        hero.equipWeapon(weapon);
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.damage(),3.33);
    }
}
