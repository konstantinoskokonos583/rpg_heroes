package TestsForAssignment1;

import Assignment_1.ItemsAndEquipment.Armor;
import Assignment_1.ItemsAndEquipment.Armor.ArmorType;
import Assignment_1.ItemsAndEquipment.Item;
import Assignment_1.ItemsAndEquipment.Item.Slot;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class ArmorMethodsTests {
    private final Item armor = new Armor("Common Cloth",4, Slot.Body, ArmorType.Cloth,1,3,5);
    @Test
    public void testConstructor(){
        Assertions.assertEquals(armor.getName(),"Common Cloth");
        Assertions.assertEquals(armor.getRequiredLevel(),4);
        Assertions.assertEquals(armor.getSlot(),Slot.Body);
        Assertions.assertEquals(((Armor)armor).getArmorType(),ArmorType.Cloth);
        Assertions.assertEquals(((Armor)armor).getArmorAttribute().getStrength(),1.0);
        Assertions.assertEquals(((Armor)armor).getArmorAttribute().getDexterity(),3.0);
        Assertions.assertEquals(((Armor)armor).getArmorAttribute().getIntelligence(),5.0);
    }

}
