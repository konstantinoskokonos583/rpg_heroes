package TestsForAssignment1;

import Assignment_1.Exceptions.InvalidArmorException;
import Assignment_1.Exceptions.InvalidWeaponException;
import Assignment_1.HeroClasses.Hero;
import Assignment_1.HeroesClasses.Warrior;
import Assignment_1.ItemsAndEquipment.Armor;
import Assignment_1.ItemsAndEquipment.Item;
import Assignment_1.ItemsAndEquipment.Weapon;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class WarriorMethodsTests{
    private final Hero hero = new Warrior("Kostas");
    @Test
    public void testConstructor(){
        Assertions.assertEquals(hero.getName(),"Kostas");
        Assertions.assertEquals(hero.getLevel(),1);
        Assertions.assertEquals(hero.getHeroAttributes().getStrength(),5.0);
        Assertions.assertEquals(hero.getHeroAttributes().getDexterity(),2.0);
        Assertions.assertEquals(hero.getHeroAttributes().getIntelligence(),1.0);
    }
    @Test
    public void testLevelUp(){
        hero.LevelUp();
        Assertions.assertEquals(hero.getLevel(),2);
        Assertions.assertEquals(hero.getHeroAttributes().getStrength(),8.0);
        Assertions.assertEquals(hero.getHeroAttributes().getDexterity(),4.0);
        Assertions.assertEquals(hero.getHeroAttributes().getIntelligence(),2.0);
    }

    @Test
    public void testEquipWeapon() throws InvalidWeaponException {
        Weapon weapon = new Weapon("Common Axe",1, Item.Slot.Weapon, Weapon.WeaponType.Axe,2);
        hero.equipWeapon(weapon);
        Assertions.assertEquals(((hero.getEquipment()).get(Item.Slot.Weapon)).getName(),"Common Axe");
    }

    @Test
    public void testEquipWeaponWithInvalidRequiredLevel() {
        Weapon weapon = new Weapon("Common Axe",2, Item.Slot.Weapon, Weapon.WeaponType.Axe,2);
        Assertions.assertThrows( InvalidWeaponException.class,() -> hero.equipWeapon(weapon));
    }

    @Test
    public void testEquipWeaponWithInvalidType() {
        Weapon weapon = new Weapon("Common Axe",1, Item.Slot.Weapon, Weapon.WeaponType.Bow,2);
        Assertions.assertThrows( InvalidWeaponException.class,() -> hero.equipWeapon(weapon));
    }

    @Test
    public void testEquipArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Plate Chest",1, Item.Slot.Body, Armor.ArmorType.Plate,1,0,0);
        hero.equipArmor(armor);
        Assertions.assertEquals(((hero.getEquipment()).get(Item.Slot.Body)).getName(),"Common Plate Chest");
    }

    @Test
    public void testEquipArmorWithInvalidRequiredLevel() {
        Armor armor = new Armor("Common Plate Chest",5, Item.Slot.Body, Armor.ArmorType.Plate,1,4,7);
        Assertions.assertThrows( InvalidArmorException.class,() -> hero.equipArmor(armor));
    }

    @Test
    public void testEquipArmorWithInvalidType() {
        Armor armor = new Armor("Common Plate Chest",5, Item.Slot.Body, Armor.ArmorType.Cloth,1,4,7);
        Assertions.assertThrows( InvalidArmorException.class,() -> hero.equipArmor(armor));
    }

    @Test
    public void testTotalAttributesWithNoEquipment(){
        Assertions.assertEquals(hero.totalAttributes().getStrength(),5.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),2.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),1.0);
    }
    @Test
    public void testTotalAttributesWithOnePieceOfArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Plate Chest",1, Item.Slot.Body, Armor.ArmorType.Plate,7,3,2);
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.totalAttributes().getStrength(),12);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),5.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),3.0);
    }

    @Test
    public void testTotalAttributesWithTwoPiecesOfArmor() throws InvalidArmorException {
        Armor armor1 = new Armor("Common Plate Chest",1, Item.Slot.Body, Armor.ArmorType.Plate,7,3,2);
        Armor armor2 = new Armor("Common Viking Hat",1, Item.Slot.Head, Armor.ArmorType.Plate,3,5,9);
        hero.equipArmor(armor1);
        hero.equipArmor(armor2);
        Assertions.assertEquals(hero.totalAttributes().getStrength(),15.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),10.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),12.0);
    }

    @Test
    public void testTotalAttributesWithReplacementOfArmor() throws InvalidArmorException {
        Armor armor1 = new Armor("Common Plate Chest",1, Item.Slot.Body, Armor.ArmorType.Plate,7,3,2);
        Armor armor2 = new Armor("Common Viking Chest",1, Item.Slot.Body, Armor.ArmorType.Plate,3,5,9);
        hero.equipArmor(armor1);
        hero.equipArmor(armor2);
        Assertions.assertEquals(hero.totalAttributes().getStrength(),8.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),7.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),10.0);
    }

    @Test
    public void testDamageWithNothing(){
        Assertions.assertEquals(hero.damage(),1.05);
    }
    @Test
    public void testDamageWithWeapon() throws InvalidWeaponException{
        Weapon weapon = new Weapon("Common Axe",1, Item.Slot.Weapon, Weapon.WeaponType.Axe,2);
        hero.equipWeapon(weapon);
        Assertions.assertEquals(hero.damage(),2.10);
    }
    @Test
    public void testDamageWithReplacementOfWeapon() throws InvalidWeaponException{
        Weapon weapon1 = new Weapon("Common Axe",1, Item.Slot.Weapon, Weapon.WeaponType.Axe,2);
        Weapon weapon2 = new Weapon("Common Hammer",1, Item.Slot.Weapon, Weapon.WeaponType.Hammer,4);
        hero.equipWeapon(weapon1);
        hero.equipWeapon(weapon2);
        Assertions.assertEquals(hero.damage(),4.20);
    }

    @Test
    public void testDamageWithArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Plate Chest",1, Item.Slot.Body, Armor.ArmorType.Plate,1,0,0);
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.damage(),1.06);
    }

    @Test
    public void testDamageWithLevelUpAndArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Plate Chest",1, Item.Slot.Body, Armor.ArmorType.Plate,1,0,0);
        hero.LevelUp();
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.damage(),1.09);
    }
    @Test
    public void testDamageWithWeaponAndArmor() throws InvalidWeaponException, InvalidArmorException {
        Weapon weapon = new Weapon("Common Axe",1, Item.Slot.Weapon, Weapon.WeaponType.Axe,2);
        Armor armor = new Armor("Common Plate Chest",1, Item.Slot.Body, Armor.ArmorType.Plate,1,0,0);
        hero.equipWeapon(weapon);
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.damage(),2.12);
    }
}
