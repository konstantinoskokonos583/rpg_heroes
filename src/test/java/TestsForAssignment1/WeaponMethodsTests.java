package TestsForAssignment1;

import Assignment_1.ItemsAndEquipment.Weapon;
import Assignment_1.ItemsAndEquipment.Weapon.WeaponType;
import Assignment_1.ItemsAndEquipment.Item;
import Assignment_1.ItemsAndEquipment.Item.Slot;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class WeaponMethodsTests {
    private final Item weapon = new Weapon("Common Axe",4, Slot.Weapon, WeaponType.Axe,5.0);
    @Test
    public void testConstructor(){
        Assertions.assertEquals(weapon.getName(),"Common Axe");
        Assertions.assertEquals(weapon.getRequiredLevel(),4);
        Assertions.assertEquals(weapon.getSlot(),Slot.Weapon);
        Assertions.assertEquals(((Weapon)weapon).getWeaponType(),WeaponType.Axe);
        Assertions.assertEquals(((Weapon)weapon).getWeaponDamage(),5.0);
    }

}
