package TestsForAssignment1;

import Assignment_1.Exceptions.InvalidArmorException;
import Assignment_1.Exceptions.InvalidWeaponException;
import Assignment_1.HeroClasses.Hero;
import Assignment_1.HeroesClasses.Mage;
import Assignment_1.ItemsAndEquipment.Armor;
import Assignment_1.ItemsAndEquipment.Item;
import Assignment_1.ItemsAndEquipment.Weapon;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class MageMethodsTests {
    private final Hero hero = new Mage("Kostas");
    @Test
    public void testConstructor(){
        Assertions.assertEquals(hero.getName(),"Kostas");
        Assertions.assertEquals(hero.getLevel(),1);
        Assertions.assertEquals(hero.getHeroAttributes().getStrength(),1.0);
        Assertions.assertEquals(hero.getHeroAttributes().getDexterity(),1.0);
        Assertions.assertEquals(hero.getHeroAttributes().getIntelligence(),8.0);
    }
    @Test
    public void testLevelUp(){
        hero.LevelUp();
        Assertions.assertEquals(hero.getLevel(),2);
        Assertions.assertEquals(hero.getHeroAttributes().getStrength(),2.0);
        Assertions.assertEquals(hero.getHeroAttributes().getDexterity(),2.0);
        Assertions.assertEquals(hero.getHeroAttributes().getIntelligence(),13.0);
    }

    @Test
    public void testEquipWeapon() throws InvalidWeaponException {
        Weapon weapon = new Weapon("Common Staff",1, Item.Slot.Weapon, Weapon.WeaponType.Staff,3);
        hero.equipWeapon(weapon);
        Assertions.assertEquals(((hero.getEquipment()).get(Item.Slot.Weapon)).getName(),"Common Staff");
    }

    @Test
    public void testEquipWeaponWithInvalidRequiredLevel() {
        Weapon weapon = new Weapon("Common Staff",2, Item.Slot.Weapon, Weapon.WeaponType.Staff,3);
        Assertions.assertThrows( InvalidWeaponException.class,() -> hero.equipWeapon(weapon));
    }

    @Test
    public void testEquipWeaponWithInvalidType() {
        Weapon weapon = new Weapon("Common Axe",1, Item.Slot.Weapon, Weapon.WeaponType.Axe,2);
        Assertions.assertThrows( InvalidWeaponException.class,() -> hero.equipWeapon(weapon));
    }

    @Test
    public void testEquipArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Cloth",1, Item.Slot.Body, Armor.ArmorType.Cloth,1,3,0);
        hero.equipArmor(armor);
        Assertions.assertEquals(((hero.getEquipment()).get(Item.Slot.Body)).getName(),"Common Cloth");
    }

    @Test
    public void testEquipArmorWithInvalidRequiredLevel() {
        Armor armor = new Armor("Common Cloth",2, Item.Slot.Body, Armor.ArmorType.Cloth,1,3,0);
        Assertions.assertThrows( InvalidArmorException.class,() -> hero.equipArmor(armor));
    }

    @Test
    public void testEquipArmorWithInvalidType() {
        Armor armor = new Armor("Common Plate",1, Item.Slot.Body, Armor.ArmorType.Plate,1,3,0);
        Assertions.assertThrows( InvalidArmorException.class,() -> hero.equipArmor(armor));
    }

    @Test
    public void testTotalAttributesWithNoEquipment(){
        Assertions.assertEquals(hero.totalAttributes().getStrength(),1.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),1.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),8.0);
    }
    @Test
    public void testTotalAttributesWithOnePieceOfArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Cloth",1, Item.Slot.Body, Armor.ArmorType.Cloth,1,3,6);
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.totalAttributes().getStrength(),2.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),4.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),14.0);
    }

    @Test
    public void testTotalAttributesWithTwoPiecesOfArmor() throws InvalidArmorException {
        Armor armor1 = new Armor("Common Cloth 1",1, Item.Slot.Body, Armor.ArmorType.Cloth,1,3,6);
        Armor armor2 = new Armor("Common Cloth 2",1, Item.Slot.Head, Armor.ArmorType.Cloth,3,2,1);
        hero.equipArmor(armor1);
        hero.equipArmor(armor2);
        Assertions.assertEquals(hero.totalAttributes().getStrength(),5.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),6.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),15.0);
    }

    @Test
    public void testTotalAttributesWithReplacementOfArmor() throws InvalidArmorException {
        Armor armor1 = new Armor("Common Cloth 1",1, Item.Slot.Body, Armor.ArmorType.Cloth,1,3,6);
        Armor armor2 = new Armor("Common Cloth 2",1, Item.Slot.Body, Armor.ArmorType.Cloth,3,2,1);
        hero.equipArmor(armor1);
        hero.equipArmor(armor2);
        Assertions.assertEquals(hero.totalAttributes().getStrength(),4.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),3.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),9.0);
    }

    @Test
    public void testDamageWithNothing(){
        Assertions.assertEquals(hero.damage(),1.08);
    }
    @Test
    public void testDamageWithWeapon() throws InvalidWeaponException{
        Weapon weapon = new Weapon("Common Staff",1, Item.Slot.Weapon, Weapon.WeaponType.Staff,3);
        hero.equipWeapon(weapon);
        Assertions.assertEquals(hero.damage(),3.24);
    }
    @Test
    public void testDamageWithReplacementOfWeapon() throws InvalidWeaponException{
        Weapon weapon1 = new Weapon("Common Staff",1, Item.Slot.Weapon, Weapon.WeaponType.Staff,3);
        Weapon weapon2 = new Weapon("Common Wand",1, Item.Slot.Weapon, Weapon.WeaponType.Wand,5);
        hero.equipWeapon(weapon1);
        hero.equipWeapon(weapon2);
        Assertions.assertEquals(hero.damage(),5.4);
    }

    @Test
    public void testDamageWithArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Cloth",1, Item.Slot.Body, Armor.ArmorType.Cloth,1,3,5);
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.damage(),1.13);
    }

    @Test
    public void testDamageWithLevelUpAndArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Cloth",1, Item.Slot.Body, Armor.ArmorType.Cloth,1,3,5);
        hero.LevelUp();
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.damage(),1.18);
    }
    @Test
    public void testDamageWithWeaponAndArmor() throws InvalidWeaponException, InvalidArmorException {
        Weapon weapon = new Weapon("Common Staff",1, Item.Slot.Weapon, Weapon.WeaponType.Staff,4);
        Armor armor = new Armor("Common Cloth",1, Item.Slot.Body, Armor.ArmorType.Cloth,4,3,5);
        hero.equipWeapon(weapon);
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.damage(),4.52);
    }
}
