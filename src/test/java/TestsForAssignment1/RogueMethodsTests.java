package TestsForAssignment1;

import Assignment_1.Exceptions.InvalidArmorException;
import Assignment_1.Exceptions.InvalidWeaponException;
import Assignment_1.HeroClasses.Hero;
import Assignment_1.HeroesClasses.Rogue;
import Assignment_1.ItemsAndEquipment.Armor;
import Assignment_1.ItemsAndEquipment.Item;
import Assignment_1.ItemsAndEquipment.Weapon;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class RogueMethodsTests {
    private final Hero hero = new Rogue("Kostas");
    @Test
    public void testConstructor(){
        Assertions.assertEquals(hero.getName(),"Kostas");
        Assertions.assertEquals(hero.getLevel(),1);
        Assertions.assertEquals(hero.getHeroAttributes().getStrength(),2.0);
        Assertions.assertEquals(hero.getHeroAttributes().getDexterity(),6.0);
        Assertions.assertEquals(hero.getHeroAttributes().getIntelligence(),1.0);
    }
    @Test
    public void testLevelUp(){
        hero.LevelUp();
        Assertions.assertEquals(hero.getLevel(),2);
        Assertions.assertEquals(hero.getHeroAttributes().getStrength(),3.0);
        Assertions.assertEquals(hero.getHeroAttributes().getDexterity(),10.0);
        Assertions.assertEquals(hero.getHeroAttributes().getIntelligence(),2.0);
    }

    @Test
    public void testEquipWeapon() throws InvalidWeaponException {
        Weapon weapon = new Weapon("Common Dagger",1, Item.Slot.Weapon, Weapon.WeaponType.Dagger,3);
        hero.equipWeapon(weapon);
        Assertions.assertEquals(((hero.getEquipment()).get(Item.Slot.Weapon)).getName(),"Common Dagger");
    }

    @Test
    public void testEquipWeaponWithInvalidRequiredLevel() {
        Weapon weapon = new Weapon("Common Dagger",2, Item.Slot.Weapon, Weapon.WeaponType.Dagger,3);
        Assertions.assertThrows( InvalidWeaponException.class,() -> hero.equipWeapon(weapon));
    }

    @Test
    public void testEquipWeaponWithInvalidType() {
        Weapon weapon = new Weapon("Common Axe",1, Item.Slot.Weapon, Weapon.WeaponType.Axe,2);
        Assertions.assertThrows( InvalidWeaponException.class,() -> hero.equipWeapon(weapon));
    }

    @Test
    public void testEquipArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Leather",1, Item.Slot.Body, Armor.ArmorType.Leather,1,4,2);
        hero.equipArmor(armor);
        Assertions.assertEquals(((hero.getEquipment()).get(Item.Slot.Body)).getName(),"Common Leather");
    }

    @Test
    public void testEquipArmorWithInvalidRequiredLevel() {
        Armor armor = new Armor("Common Leather",2, Item.Slot.Body, Armor.ArmorType.Leather,1,4,2);
        Assertions.assertThrows( InvalidArmorException.class,() -> hero.equipArmor(armor));
    }

    @Test
    public void testEquipArmorWithInvalidType() {
        Armor armor = new Armor("Common Plate",1, Item.Slot.Body, Armor.ArmorType.Plate,1,3,0);
        Assertions.assertThrows( InvalidArmorException.class,() -> hero.equipArmor(armor));
    }

    @Test
    public void testTotalAttributesWithNoEquipment(){
        Assertions.assertEquals(hero.totalAttributes().getStrength(),2.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),6.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),1.0);
    }
    @Test
    public void testTotalAttributesWithOnePieceOfArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Leather",1, Item.Slot.Body, Armor.ArmorType.Leather,1,4,2);
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.totalAttributes().getStrength(),3.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),10.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),3.0);
    }

    @Test
    public void testTotalAttributesWithTwoPiecesOfArmor() throws InvalidArmorException {
        Armor armor1 = new Armor("Common Leather 1",1, Item.Slot.Body, Armor.ArmorType.Leather,1,4,2);
        Armor armor2 = new Armor("Common Leather 2",1, Item.Slot.Head, Armor.ArmorType.Leather,3,2,4);
        hero.equipArmor(armor1);
        hero.equipArmor(armor2);
        Assertions.assertEquals(hero.totalAttributes().getStrength(),6.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),12.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),7.0);
    }

    @Test
    public void testTotalAttributesWithReplacementOfArmor() throws InvalidArmorException {
        Armor armor1 = new Armor("Common Leather 1",1, Item.Slot.Body, Armor.ArmorType.Leather,1,4,2);
        Armor armor2 = new Armor("Common Leather 2",1, Item.Slot.Body, Armor.ArmorType.Leather,3,2,4);
        hero.equipArmor(armor1);
        hero.equipArmor(armor2);
        Assertions.assertEquals(hero.totalAttributes().getStrength(),5.0);
        Assertions.assertEquals(hero.totalAttributes().getDexterity(),8.0);
        Assertions.assertEquals(hero.totalAttributes().getIntelligence(),5.0);
    }

    @Test
    public void testDamageWithNothing(){
        Assertions.assertEquals(hero.damage(),1.06);
    }
    @Test
    public void testDamageWithWeapon() throws InvalidWeaponException{
        Weapon weapon = new Weapon("Common Dagger",1, Item.Slot.Weapon, Weapon.WeaponType.Dagger,3);
        hero.equipWeapon(weapon);
        Assertions.assertEquals(hero.damage(),3.18);
    }
    @Test
    public void testDamageWithReplacementOfWeapon() throws InvalidWeaponException{
        Weapon weapon1 = new Weapon("Common Dagger",1, Item.Slot.Weapon, Weapon.WeaponType.Dagger,3);
        Weapon weapon2 = new Weapon("Common Sword",1, Item.Slot.Weapon, Weapon.WeaponType.Sword,4);
        hero.equipWeapon(weapon1);
        hero.equipWeapon(weapon2);
        Assertions.assertEquals(hero.damage(),4.24);
    }

    @Test
    public void testDamageWithArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Leather",1, Item.Slot.Body, Armor.ArmorType.Leather,1,4,2);
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.damage(),1.10);
    }

    @Test
    public void testDamageWithLevelUpAndArmor() throws InvalidArmorException {
        Armor armor = new Armor("Common Leather",1, Item.Slot.Body, Armor.ArmorType.Leather,2,3,3);
        hero.LevelUp();
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.damage(),1.13);
    }
    @Test
    public void testDamageWithWeaponAndArmor() throws InvalidWeaponException, InvalidArmorException {
        Weapon weapon = new Weapon("Common Dagger",1, Item.Slot.Weapon, Weapon.WeaponType.Dagger,4);
        Armor armor = new Armor("Common Leather",1, Item.Slot.Body, Armor.ArmorType.Leather,2,3,3);
        hero.equipWeapon(weapon);
        hero.equipArmor(armor);
        Assertions.assertEquals(hero.damage(),4.36);
    }
}
